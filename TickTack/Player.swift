//
//  Player.swift
//  TickTack
//
//  Created by A14Z8R5R on 21/12/18.
//  Copyright © 2018 MutualMobile. All rights reserved.
//

import Foundation

enum PlayerType {
    case X,O
    
    mutating func toggle() {
        switch self {
        case .X:
            self = .O
        case .O:
            self = .X
        }
    }
}

class Player {
    private (set) var winnerIndexPaths = [IndexPath]()
    private (set) var state = [IndexPath : String]()
    private (set) var activePlayer = PlayerType.X
    let size: Int
    
    private lazy var winnerX = String(Array(repeating: "X", count: size))
    private lazy var winnerO = String(Array(repeating: "O", count: size))
    
    init(size: Int) {
        self.size = size
    }
    
    func resetGame() {
        state.removeAll()
        winnerIndexPaths.removeAll()
    }
    
    func toggleActivePlayer(){
        activePlayer.toggle()
    }
    
    func updatePlayerStateIfRequired(with indexPath: IndexPath) -> Bool
    {
        if state[indexPath] == nil
        {
            switch activePlayer {
            case .X:
                state[indexPath] = "X"
            case .O:
                state[indexPath] = "O"
            }
            return true
        }
        return false
    }
    
    func findWinner() throws -> PlayerType {
        
        if let winner = search(by: .row){
            return winner
        }
        
        if let winner = search(by: .col){
            return winner
        }
        
        if let winner = search(by: .diagonalLeftToRight){
            return winner
        }
        
        if let winner = search(by: .diagonalRightToLeft){
            return winner
        }
        
        throw (state.keys.count == size * size) ? WinnerError.noWinner : WinnerError.stillInProgress
    }
    
    private enum SearchType {
        case row, col, diagonalLeftToRight, diagonalRightToLeft
    }
    
    private func winnerPlayerType(_ result: String) -> PlayerType?
    {
        if result == winnerX{
            return .X
        }
        else if result == winnerO{
            return .O
        }
        return nil
    }
    
    private func search(by type: SearchType) -> PlayerType?
    {
        if type == .row || type == .col
        {
            for row in 0..<size
            {
                var result = ""
                var winPaths = [IndexPath]()
                for col in 0..<size
                {
                    let rowValue = (type == .row) ? col : row
                    let sectionValue = (type == .row) ? row : col
                    let indexPath = IndexPath(row: rowValue, section: sectionValue)
                    let value = state[indexPath] ?? ""
                    result.append(value)
                    winPaths.append(indexPath)
                }
                
                if let winner = winnerPlayerType(result){
                    winnerIndexPaths = winPaths
                    return winner
                }
            }
        }
        else if type == .diagonalLeftToRight || type == .diagonalRightToLeft
        {
            var result = ""
            var winPaths = [IndexPath]()
            for row in 0..<size
            {
                let rowValue = row
                let sectionValue = (type == .diagonalLeftToRight) ? row : (size - 1 - row)
                let indexPath = IndexPath(row: rowValue, section: sectionValue)
                let value = state[indexPath] ?? ""
                result.append(value)
                winPaths.append(indexPath)
            }
            
            if let winner = winnerPlayerType(result){
                winnerIndexPaths = winPaths
                return winner
            }
        }
        
        return nil
    }
}

enum WinnerError: Error {
    case stillInProgress
    case noWinner
}
