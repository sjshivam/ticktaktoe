//
//  TickTackCollectionViewCell.swift
//  TickTack
//
//  Created by A14Z8R5R on 21/12/18.
//  Copyright © 2018 MutualMobile. All rights reserved.
//

import UIKit

class TickTackCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var displayLabel: UILabel!
    let bgColor = UIColor.init(red: 223.0/250.0, green: 255.0/255.0, blue: 110.0/255.0, alpha: 1.0)
    let winColor = UIColor.init(red: 192.0/250.0, green: 192.0/255.0, blue: 192.0/255.0, alpha: 1.0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        self.contentView.layer.borderWidth = 0.5
        self.contentView.layer.borderColor = UIColor.black.cgColor
        
        displayLabel.adjustsFontSizeToFitWidth = true;
    }
    
    func updateText(_ text: String?){
        displayLabel.text = text
    }
}
