//
//  ViewController.swift
//  TickTack
//
//  Created by A14Z8R5R on 21/12/18.
//  Copyright © 2018 MutualMobile. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet weak var collectionView: UICollectionView!
    
    let player = Player(size: 3)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.sectionInset = .init(top: 0, left: 0, bottom: 0, right: 0)
        
        collectionView.collectionViewLayout = layout
        collectionView.delegate = self
        collectionView.dataSource = self
        
        title = displayTitle()
    }
    
    private func resetGame() {
        player.resetGame()
        collectionView.reloadData()
        title = displayTitle()
    }
    
    func displayTitle() -> String {
        switch player.activePlayer {
        case .X:
            return "Player X's chance"
        case .O:
            return "Plyer O's chance"
        }
    }
    
    func winnerTitle(_ winner: PlayerType) -> String {
        switch winner {
        case .X:
            return "Winner is X"
        case .O:
            return "Winner is O"
        }
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    // MARK: collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return player.size
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return player.size
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TickTackCollectionViewCell", for: indexPath) as! TickTackCollectionViewCell
        let value = player.state[indexPath]
        cell.updateText(value)
        cell.backgroundColor = player.winnerIndexPaths.contains(indexPath) ? cell.winColor : cell.bgColor
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let required = player.updatePlayerStateIfRequired(with: indexPath)
        if required
        {
            player.toggleActivePlayer()
            title = displayTitle()
            do
            {
                let winner = try player.findWinner()
                title = winnerTitle(winner)
                collectionView.reloadItems(at: player.winnerIndexPaths)
                
                DispatchQueue.main.asyncAfter(wallDeadline: .now() + 1) { [weak self] in
                    self?.showAlertForNewGame(message: self?.title)
                }
            }
            catch WinnerError.noWinner
            {
                collectionView.reloadItems(at: [indexPath])
                showAlertForNewGame(message: "No one won!!")
            }
            catch _ {   
                collectionView.reloadItems(at: [indexPath])
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.size.width/CGFloat(player.size)
        return CGSize(width: width, height: width)
    }
}

extension ViewController
{
    private func showAlertForNewGame(message: String?) {
        let alert = UIAlertController(title: "New Game ?", message: message, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Okay", style: .default) {[weak self] (_) in
            self?.resetGame()
        }
        alert.addAction(confirmAction)
        present(alert, animated: true, completion: nil)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView.reloadData()
    }
}

